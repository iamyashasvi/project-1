const fs = require("fs");
var year;
var count = 0;
module.exports.matchPerYear = function matchPerYear(matches) {
    count = 0;
    var match = {};
    var temp = matches.reduce((acc, val) => {
        if (acc.hasOwnProperty(val.season)) {
            acc[val.season] += 1;
        } else {
            acc[val.season] = 1;
        }
        return acc;
    }, {});
    fs.writeFile("../output/matchPerYear.json", JSON.stringify(temp), (err) => {
        if (err) throw err;
    });
}


module.exports.matchesWonPerTeam = function matchesWonPerTeam(matches) {
    count = 0;
    var team = {};
    var temp = {};
    team = matches.reduce((acc, val) => {
        if (acc.hasOwnProperty(val.winner)) {
            if (acc[val.winner].hasOwnProperty(val.season)) {
                acc[val.winner][val.season] += 1;
            } else {
                acc[val.winner][val.season] = 1;
            }
        } else {
            acc[val.winner] = {};
            acc[val.winner][val.season] = 1;
        }
        return acc;
    }, {});
    var newarr = [];
    for (let key in team) {
        var arr = [];
        var objt = {};
        if (key !== '') {
            objt['name'] = key;
            for (let j = 2008; j < 2018; j++) {
                if (team[key].hasOwnProperty(j)) {
                    arr.push(team[key][j])
                } else {
                    arr.push(0);
                }
                objt['data'] = arr;
            }
            newarr.push(objt);
        }
    }
    fs.writeFile("../output/matchesWonPerTeam.json", JSON.stringify(newarr), (err) => {
        if (err) throw err;
    });
}


module.exports.runConceded = function runConceded(year = 2016, matches, delivery) {
    count = 0;
    var matchid = {};
    var conced = {};
    for (let i = 0; i < matches.length; i++) {
        if (matches[i].season == year) {
            matchid[matches[i].id] = 1;
        }
    }
    for (let i = 0; i < delivery.length; i++) {
        if (matchid[delivery[i].match_id] !== undefined) {
            // console.log("he;")
            if (conced[delivery[i].bowling_team] !== undefined) {
                conced[delivery[i].bowling_team] += delivery[i].extra_runs;
            } else {
                conced[delivery[i].bowling_team] = delivery[i].extra_runs;
            }
        }
    }
    fs.writeFile("../output/runConceded.json", JSON.stringify(conced), (err) => {
        if (err) throw err;
    });

}


module.exports.topEconomicalBowler = function topEconomicalBowler(year = 2015, matches, delivery) {
    count = 0;
    var matchid = {};
    var conced = {};
    var finalval = {};
    for (let i = 0; i < matches.length; i++) {
        if (matches[i].season == year) {
            matchid[matches[i].id] = 1;
        }
    }
    for (let i = 0; i < delivery.length; i++) {
        if (matchid[delivery[i].match_id] !== undefined) {
            if (conced[delivery[i].bowler] !== undefined) {
                if (conced[delivery[i].bowler]['ball'] !== undefined) {
                    if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                        conced[delivery[i].bowler]['ball']++;
                    }
                    if (conced[delivery[i].bowler]['total_runs'] !== undefined) {
                        conced[delivery[i].bowler]['total_runs'] += delivery[i].total_runs - (delivery[i].legbye_runs + delivery[i].bye_runs);
                    } else {
                        conced[delivery[i].bowler]['total_runs'] = delivery[i].total_runs;
                    }
                } else {
                    if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                        conced[delivery[i].bowler]['ball'] += 1;
                    }
                    conced[delivery[i].bowler]['total_runs'] += delivery[i].total_runs - delivery[i].legbye_runs - delivery[i].bye_runs;
                }
            } else {
                conced[delivery[i].bowler] = {};
                conced[delivery[i].bowler]['ball'] = 0;
                conced[delivery[i].bowler]['total_runs'] = 0;
                if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                    conced[delivery[i].bowler]['ball'] = 1;
                }
                conced[delivery[i].bowler]['total_runs'] = delivery[i].total_runs - delivery[i].legbye_runs - delivery[i].bye_runs;
            }
        }
    }
    for (let keys in conced) {
        conced[keys]['economist'] = (conced[keys]['total_runs'] / conced[keys]['ball']) * 6;
    }
    var arr = [];
    for (let keys in conced) {
        arr.push([keys, conced[keys]['economist']]);
    }
    arr.sort(function(a, b) {
        return a[1] - b[1];
    });
    var top = arr.slice(0, 10);
    var objs = top.map(function(x) {
        return {
            name: x[0],
            y: x[1]
        };
    });
    fs.writeFile("../output/topEconomicalBowler.json", JSON.stringify(objs), (err) => {
        if (err) throw err;
    });
}




module.exports.winTossAndMatch = function winTossAndMatch(matches) {
    var ans = {};
    var arr = [];
    for (let i = 0; i < matches.length; i++) {
        if (matches[i].toss_winner === matches[i].winner) {
            if (ans.hasOwnProperty(matches[i].toss_winner)) {
                ans[matches[i].toss_winner] += 1;
            } else {
                ans[matches[i].toss_winner] = 1;
            }
        }
    }
    for (var number in ans) {
        arr.push([number, ans[number]])
    }
    fs.writeFile("../output/winTossAndMatch.json", JSON.stringify(arr), (err) => {
        if (err) throw err;
    });
}



module.exports.playerOfMatchAward = function playerOfMatchAward(matches) {
    var player = {};
    var playerOfTheMatch = {};
    var arr = [];
    for (let i = 0; i < matches.length; i++) {
        if (player.hasOwnProperty(matches[i].season)) {
            if (player[matches[i].season].hasOwnProperty(matches[i].player_of_match)) {
                player[matches[i].season][matches[i].player_of_match] += 1;
            } else {
                player[matches[i].season][matches[i].player_of_match] = 1;
            }
        } else {
            player[matches[i].season] = {};
        }
    }
    var max = 0;
    for (let key in player) {
        for (let play in player[key]) {
            if (max < player[key][play]) {
                playerOfTheMatch[key] = {}
                playerOfTheMatch[key][play] = player[key][play];
                max = player[key][play];
                arr.push()
            }
        }
        max = 0;
    }
    for (let i in playerOfTheMatch) {
        for (let j in playerOfTheMatch[i]) {
            arr.push([j, playerOfTheMatch[i][j]])
        }
    }
    var objs = arr.map(function(x) {
        return {
            name: x[0],
            low: x[1]
        };
    });
    fs.writeFile("../output/playerOfMatchAward.json", JSON.stringify(objs), (err) => {
        if (err) throw err;
    });
}



module.exports.strikeRateKohli = function strikeRateKohli(player = 'V Kohli', matches, delivery) {
    count = 0;
    var player = 'V Kohli';
    var ball = {};
    var run = {};
    var matchid = {};
    var result = {};
    for (let i = 0; i < matches.length; i++) {
        matchid[matches[i].id] = matches[i].season;
    }
    for (let i = 0; i < delivery.length; i++) {
        obj = delivery[i];
        if (obj.match_id in matchid) {
            if (ball.hasOwnProperty(matchid[obj.match_id]) && obj.batsman === player && obj.wide_runs == 0) {
                ball[matchid[obj.match_id]] += 1;
                if (obj.noball_runs != 0) {
                    ball[matchid[obj.match_id]] += 1;
                }
            } else {
                if (obj.batsman === player && obj.wide_runs == 0) {
                    ball[matchid[obj.match_id]] = 1;
                }
            }
            if (run.hasOwnProperty(matchid[obj.match_id])) {
                if (obj.batsman === player) {
                    run[matchid[obj.match_id]] += obj.batsman_runs;
                }
            } else {
                if (obj.batsman === player) {
                    run[matchid[obj.match_id]] = obj.batsman_runs;
                }
            }
        }
    }
    for (let key in run) {
        result[key] = (run[key] / ball[key]) * 100;
    }
    fs.writeFile("../output/strikeRateKohli.json", JSON.stringify(result), (err) => {
        if (err) throw err;
    });
}

module.exports.highestDismissedPlayer = function highestDismissedPlayer(delivery) {
    var dismissed = {};
    var player_dismissed = 0;
    for (let i = 0; i < delivery.length; i++) {
        if ((delivery[i].player_dismissed) !== '' && delivery[i].dismissal_kind !== 'run out') {
            if (dismissed.hasOwnProperty(delivery[i].player_dismissed)) {
                if (dismissed[delivery[i].player_dismissed].hasOwnProperty(delivery[i].batsman) || dismissed[delivery[i].player_dismissed].hasOwnProperty(delivery[i].bowler)) {
                    if (delivery[i].player_dismissed === delivery[i].batsman) {
                        dismissed[delivery[i].player_dismissed][delivery[i].bowler]++;
                    } else {
                        dismissed[delivery[i].player_dismissed][delivery[i].batsman]++;
                    }
                } else {
                    if (delivery[i].player_dismissed === delivery[i].batsman) {
                        dismissed[delivery[i].player_dismissed][delivery[i].bowler] = 1;
                    } else {
                        dismissed[delivery[i].player_dismissed][delivery[i].batsman] = 1;
                    }
                }
            } else {
                dismissed[delivery[i].player_dismissed] = {};
                if (delivery[i].player_dismissed === delivery[i].batsman) {
                    dismissed[delivery[i].player_dismissed][delivery[i].bowler] = 1;
                } else {
                    dismissed[delivery[i].player_dismissed][delivery[i].batsman] = 1;
                }
            }
        }
    }
    var max = 0;
    var arr = [];
    for (let keys in dismissed) {
        for (let subkey in dismissed[keys]) {
            if (max <= dismissed[keys][subkey]) {
                arr.push([keys, subkey, dismissed[keys][subkey]]);
                max = dismissed[keys][subkey];
            }
        }
    }
    arr = arr.slice(-10);
    var newarr = [];
    for (let i = 0; i < arr.length; i++) {
        newarr.push([arr[i][0], arr[i][2]])
    }
    fs.writeFile("../output/highestDismissedPlayer.json", JSON.stringify(newarr), (err) => {
        if (err) throw err;
    });
}


module.exports.bestEconomySuperOver = function bestEconomySuperOver(delivery) {
    count = 0;
    var conced = {};
    for (let i = 0; i < delivery.length; i++) {
        if (delivery[i].is_super_over != 0) {
            if (conced[delivery[i].bowler] !== undefined) {
                if (conced[delivery[i].bowler]['ball'] !== undefined) {
                    if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                        conced[delivery[i].bowler]['ball']++;
                    }
                    if (conced[delivery[i].bowler]['total_runs'] !== undefined) {
                        conced[delivery[i].bowler]['total_runs'] += delivery[i].total_runs - (delivery[i].legbye_runs + delivery[i].bye_runs);
                    } else {
                        conced[delivery[i].bowler]['total_runs'] = delivery[i].total_runs;
                    }
                } else {
                    if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                        conced[delivery[i].bowler]['ball'] += 1;
                    }
                    conced[delivery[i].bowler]['total_runs'] += delivery[i].total_runs - delivery[i].legbye_runs - delivery[i].bye_runs;
                }
            } else {
                conced[delivery[i].bowler] = {};
                conced[delivery[i].bowler]['ball'] = 0;
                conced[delivery[i].bowler]['total_runs'] = 0;
                if (delivery[i].wide_runs == 0 && delivery[i].noball_runs == 0) {
                    conced[delivery[i].bowler]['ball'] = 1;
                }
                conced[delivery[i].bowler]['total_runs'] = delivery[i].total_runs - delivery[i].legbye_runs - delivery[i].bye_runs;
            }
        }
    }
    for (let keys in conced) {
        conced[keys]['economist'] = (conced[keys]['total_runs'] / conced[keys]['ball']) * 6;
    }
    var arr = [];
    for (let keys in conced) {
        arr.push([keys, conced[keys]['economist']]);
    }
    arr.sort(function(a, b) {
        return a[1] - b[1];
    });
    var top = arr.slice(0, 10);
    var objs = arr.map(function(x) {
        return {
            name: x[0],
            low: x[1]
        };
    });
    fs.writeFile("../output/bestEconomySuperOver.json", JSON.stringify(objs), (err) => {
        if (err) throw err;
    });
}