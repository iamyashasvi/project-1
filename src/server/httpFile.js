 
function httpFile(){
    var http = require('http');
    var url = require('url');
    var fs = require('fs');
    var server = http.createServer(function(req,res){
    var q = url.parse(req.url,true);
    var indexPage = "./index.html";
    var filename = "../output"+q.pathname+".json";
    if(q.pathname==="/"){
        fs.readFile(indexPage,function(err,data){
            if(err){
                res.writeHead(404,{'content-Type':'text/html'});
                return res.end("404 Not Found");
            }
            res.writeHead(200,{'content-Type':'text/html'});
            res.write(data);
            res.write("Which file do you go..<br>");
            res.write("1. matchesWonPerTeam<br>");
            res.write("2. runConceded<br>");
            res.write('3. topEconomicalBowler<br>');
            res.write("4. matchPerYear<br>");
            res.write("5. winTossAndMatch<br>");
            res.write("6. playerOfMatchAward<br>");
            res.write("7. strikeRateKohli<br>");
            res.write("8. highestDismissedPlayer<br>");
            res.write("9. bestEconomySuperOver<br>")
            res.end();
        });
    }
    else{
        fs.readFile(filename,function(err,data){
            if(err){
                res.writeHead(404,{'content-Type':'application/JSON'});
                return res.end("404 Not Found");
            }
            res.writeHead(200,{'content-Type':'application/JSON'});
            res.write(data);
            return res.end();
        });
    }
});
server.listen(8080,'127.0.0.1');
}

httpFile();
