const fs = require("fs");
const csvFilePath = '../data/matches.csv';
const csv = require('csvtojson');
const csvFilePath1 = '../data/deliveries.csv';
const ipl = require('./ipl.js');
csv({
    colParser: {
        "season": "number"
    },
    checkType: true
}).fromFile(csvFilePath).then((matches) => {
    csv({
        colParser: {
            "batsman_runs": "number",
            "extra_runs": "number",
            "bye_runs": "number",
            "total_runs": "number",
            "noball_runs": "number",
            "wide_runs": "number",
            "legbye_runs": "number",
            "is_super_over": "number",
        },
        checkType: true
    }).fromFile(csvFilePath1).then((delivery) => {
        ipl.matchPerYear(matches);
        ipl.matchesWonPerTeam(matches);
        ipl.runConceded(2016, matches, delivery);
        ipl.topEconomicalBowler(2015, matches, delivery);
        ipl.winTossAndMatch(matches);
        ipl.playerOfMatchAward(matches);
        ipl.strikeRateKohli('V Kohli', matches, delivery);
        ipl.highestDismissedPlayer(delivery);
        ipl.bestEconomySuperOver(delivery);
    });
});