 function server() {
     var http = require('http');
     var url = require('url');
     var fs = require('fs');
     var server = http.createServer(function(req, res) {
         var q = url.parse(req.url, true);
         switch (q.pathname) {
             case ("/"):
                 fs.readFile('./index.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/runConceded"):
                 fs.readFile('./runConceded.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/runConceded.json"):
                 fs.readFile('../output/runConceded.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ('/matchPerYear'):
                 fs.readFile('./matchPerYear.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ('/matchPerYear.json'):
                 fs.readFile('../output/matchPerYear.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/winTossAndMatch"):
                 fs.readFile('winTossAndMatch.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/winTossAndMatch.json"):
                 fs.readFile('../output/winTossAndMatch.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/strikeRateKohli"):
                 fs.readFile('strikeRateKohli.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/strikeRateKohli.json"):
                 fs.readFile('../output/strikeRateKohli.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/bestEconomySuperOver"):
                 fs.readFile('bestEconomySuperOver.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/bestEconomySuperOver.json"):
                 fs.readFile('../output/bestEconomySuperOver.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/matchesWonPerTeam"):
                 fs.readFile('matchesWonPerTeam.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/matchesWonPerTeam.json"):
                 fs.readFile('../output/matchesWonPerTeam.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/topEconomicalBowlers"):
                 fs.readFile('./topEconomicalBowler.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/topEconomicalBowler.json"):
                 fs.readFile('../output/topEconomicalBowler.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/playerOfMatchAward"):
                 fs.readFile('./playerOfMatchAward.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/playerOfMatchAward.json"):
                 fs.readFile('../output/playerOfMatchAward.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;

             case ("/highestDismissedPlayer"):
                 fs.readFile('./highestDismissedPlayer.html', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'text/html' });
                     res.write(data);
                     res.end();
                 });
                 break;
             case ("/highestDismissedPlayer.json"):
                 fs.readFile('../output/highestDismissedPlayer.json', function(err, data) {
                     if (err) {
                         res.writeHead(404, { 'content-Type': 'text/html' });
                         return res.end("404 Not Found");
                     }
                     res.writeHead(200, { 'content-Type': 'application/JSON' });
                     res.write(data);
                     res.end();
                 });
                 break;
             default:
                 res.write("oops you are in wrong way");
                 res.end();
                 break;
         }
     });
     server.listen(8080, '127.0.0.1');
 }
 server();